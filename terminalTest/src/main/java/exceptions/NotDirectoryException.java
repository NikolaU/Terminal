package exceptions;

public class NotDirectoryException extends Exception{

	public NotDirectoryException(String poruka) {
		super(poruka);
	}
	
}
