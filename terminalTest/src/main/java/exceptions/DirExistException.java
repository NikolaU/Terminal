package exceptions;

public class DirExistException extends Exception {

	public DirExistException(String poruka) {
		super(poruka);
	}
	
}
