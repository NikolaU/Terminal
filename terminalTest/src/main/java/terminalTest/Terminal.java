package terminalTest;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import exceptions.DirExistException;
import exceptions.NotDirectoryException;

public class Terminal {

	public static void main(String[] args) throws DirExistException, NotDirectoryException {

		try {
			BufferedReader bw = new BufferedReader(new InputStreamReader(System.in));
			String red;
			
			//Inicijalizacija objekata
			Putanje putanje = new Putanje();
			Kreiranje kreiranje = new Kreiranje();
			Brisanje brisanje = new Brisanje();
			KopiranjePomeranje copyMove = new KopiranjePomeranje();
			
			System.out.print("Unesite komandu: ");

			while ((red = bw.readLine()) != null) {
				if(red.contains("exit")){
					System.out.print("Izlazite iz terminala");
					System.exit(0);
				}
				else if(red.contains("pwd")){
					putanje.pwdCommand();
					System.out.print("Unesite komandu: ");
				}
				else if(red.contains("lst")){
					String putanja = "C:\\Users\\nikola.uzelac\\workspace1";
					putanje.lstCommand(putanja);
					System.out.print("Unesite komandu: ");
				}
				else if(red.contains("mkdir")){
					kreiranje.mkdirCommand();
					System.out.print("Unesite komandu: ");
				}
				else if(red.contains("mkfile")){
					kreiranje.mkfileCommand();
					System.out.print("Unesite komandu: ");
				}
				else if(red.contains("rdmir")){
					brisanje.rdmirCommand();
					System.out.print("Unesite komandu: ");
				}
				else if(red.contains("rmfile")){
					brisanje.rmfileCommand();
					System.out.print("Unesite komandu: ");
				}
				else if(red.contains("copy")){
					System.out.print("Unesite naziv fajla koji zelite da kopirate: ");
					String fajl = bw.readLine();
					System.out.print("Unesite naziv direktorijuma u koji zelite da premestite fajl: ");
					String dir = bw.readLine();
					copyMove.copy(fajl, dir);
					System.out.println("Unesite komandu: ");
				}
				else if(red.contains("move")){
					System.out.print("Unesite naziv fajla ili direktorijuma koji zelite da premestite: ");
					String ime = bw.readLine();
					System.out.print("Unesite naziv direktorijuma u koji zelite da premestite fajl/direktorijum: ");
					String imeDir = bw.readLine();
					copyMove.move(ime, imeDir);
					System.out.println("Unesite komandu: ");
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println("Fajl nije pronadjen!");
		} catch (IOException e) {
			System.out.println("Greska pri iscitavanju iz fajla!");
		}
	}
}