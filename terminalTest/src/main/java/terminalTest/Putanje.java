package terminalTest;

import java.io.File;

public class Putanje {

	//Apsolutna putanja za direktorijum
	public void pwdCommand() {
		File file = new File(".");
		System.out.println(file.getAbsolutePath());
	}

	//Izlistavanje root direktorijuma
	public void lstCommand(String putanja) {
		if (putanja == null) {	
			File file = new File(".");
			if (file.isDirectory()) {
				String[] files = file.list();
				System.out.println(file.getAbsolutePath());
				for(String fajl:files){
					System.out.println(fajl);
				}	
			}
		}
	}
	
}
