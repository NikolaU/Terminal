package terminalTest;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import exceptions.NotDirectoryException;

public class KopiranjePomeranje {

	// Kopiranje fajla u direktorijum
	public void copy(String trenutnaPutanja, String putanjaKopiranja) {

		File sourceFile = new File(trenutnaPutanja);
		File fileDir = new File(putanjaKopiranja);
		if (fileDir.isDirectory()) {
			try {
				FileUtils.copyFileToDirectory(sourceFile, fileDir);
				System.out.println("Fajl kopiran iz: " + sourceFile.getName() + " premesten u " + fileDir.getName());
			} catch (FileNotFoundException e) {
				System.out.println("Fajl nije pronadjen (klasa: KopiranjePomeranje)");
			} catch (IOException e) {
				System.out.println("Greska pri iscitavanju iz fajla");
			}
		}
	}

	// Pomeranje fajla/direktorijuma u direktorijum
	public void move(String trenutnaPutanja, String putanjaKopiranja) throws NotDirectoryException, IOException {

		File sourceFile = new File(trenutnaPutanja);
		File fileDir = new File(putanjaKopiranja);
		if (!fileDir.isDirectory()) {
			throw new NotDirectoryException("Ne mozete kopirati u fajl koji nije direktorijum!");
		} else {
			if (sourceFile.exists()) {
				if (sourceFile.isDirectory()) {
					FileUtils.moveDirectory(sourceFile, fileDir);
					System.out.println("Direktorijum premesten u " + fileDir.getName());
				} else if (sourceFile.isFile()) {
					FileUtils.moveFileToDirectory(sourceFile, fileDir, true);
					System.out.println("Fajl premesten u " + fileDir.getName());
				}
			}
			else{
				throw new FileNotFoundException("Fajl nije pronadjen!");
			}
		}
	}

}