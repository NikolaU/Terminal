package terminalTest;

import java.io.File;
import java.util.Scanner;

public class Brisanje {

	Scanner t = new Scanner(System.in);
	
	//Brisanje direktorijuma
	public void rdmirCommand(){
		System.out.print("Unesite naziv direktorijuma koji zelite da obrisete: ");
		String direktorijum = t.nextLine();
		File file = new File(direktorijum);
		String[] files = file.list();
		if(files.length==0){
			file.delete();
			System.out.println("Obrisan direktorijum: "+file.getName());
		}
		else{
			System.out.println("Ne mozete obrisati direktorijum sa podfolderima!");
		}
	}
	
	//Brisanje fajla
	public void rmfileCommand(){	
		System.out.print("Unesite ime fajla koji zelite da obrisete: ");
		String fajl = t.nextLine();
		System.out.print("Da li ste sigurni da zelite da obrisete fajl? ");
		String potvrda;
		if((potvrda=t.nextLine()).contains("da")){			
			File file = new File(fajl);
			file.delete();
			System.out.println("Fajl je obrisan.");
		}
		else
			System.out.println("Fajl nije obrisan!");
	}
	
}
