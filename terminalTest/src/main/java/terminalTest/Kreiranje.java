package terminalTest;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import exceptions.DirExistException;

public class Kreiranje {

	//Kreiranje direktorijuma
	public void mkdirCommand() throws DirExistException{
		Scanner t = new Scanner(System.in);
		System.out.print("Unesite naziv novog direktorijuma: ");
		String direktorijum=t.nextLine();
		File file = new File(direktorijum);
		if(file.exists()){
			throw new DirExistException("Vec postoji direktorijum pod istim imenom!");		
		}
		else{
			file.mkdirs();
			System.out.println("Kreiran direktorijum po nazivom: "+file.getName());
		}
	}
	
	//Kreiranje fajla
	public void mkfileCommand() throws IOException{
		Scanner t = new Scanner(System.in);
		System.out.print("Unesite naziv novog fajla: ");
		String fajl=t.nextLine();
		File file = new File(fajl);
		if(file.exists()){
			System.out.println("Fajl sa istim imenom vec postoji");
		}
		else{
			file.createNewFile();
			System.out.println("Kreiran novi fajl sa imenom: "+file.getName());
		}
	}
	
}
